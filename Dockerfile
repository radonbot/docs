FROM python:buster
RUN apt update
RUN apt install -y nginx
COPY requirements.txt /docs/requirements.txt
RUN pip3 install -r /docs/requirements.txt
COPY build.sh /docs/build.sh
RUN mkdir -p /docs/source
RUN mkdir /src
RUN chmod +x /docs/build.sh
RUN mkdir /app
RUN echo "{}" > /app/config.json
COPY sphinx_config.py /docs/source/conf.py