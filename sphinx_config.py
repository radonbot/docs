import sys
import sphinx_rtd_theme

sys.path.insert(0, "/src")
extensions = ["sphinxcontrib.napoleon", "sphinx_autodoc_typehints",
              "sphinx_rtd_theme"]
master_doc = "bot"

html_theme = "sphinx_rtd_theme"
