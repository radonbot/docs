#!/bin/bash
cd /docs
sphinx-apidoc -P -f -o /docs/source /src
sphinx-build -b html /docs/source/ /docs/build/
nginx